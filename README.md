
# UnoSync

UnoSync is an Arduino-Uno based clock-sync-box that allows individual synchronized start/stop.

More information, see the [project page](https://rv0.be/projects/unosync) on rv0.be.


## Changelog

### Version 0.1
 
- Initial version


## License

[UnoSync](https://rv0.be/projects/unosync) © 2022 by [rv0](https://rv0.be/) is licensed under  [CC BY-NC 4.0](http://creativecommons.org/licenses/by-nc/4.0/?ref=chooser-v1)