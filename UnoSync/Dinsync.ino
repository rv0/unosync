void handleDinsyncStart(int port) {
  if (dinsyncClockHigh) { // Destroy any active clocks. 
    setDinsyncClockLow();
  }

  dinsyncNeedsDelay = true; // Doesn't matter if this gets called more than once, also, the delay is global.

  tmp_portc = 0;
  tmp_portd = 0;
  switch (port) {
    case 1:
      bitSet(tmp_portc, DINSYNC_RUN1_BIT);
      bitSet(tmp_portd, LED_DINSYNC1_BIT);
      break;
    case 2:
      bitSet(tmp_portc, DINSYNC_RUN2_BIT);
      bitSet(tmp_portd, LED_DINSYNC2_BIT);      
      break;
    case 3:
      bitSet(tmp_portc, DINSYNC_RUN3_BIT);
      bitSet(tmp_portd, LED_DINSYNC3_BIT);
      break;
    case 4:
      bitSet(tmp_portc, DINSYNC_RUN4_BIT);
      bitSet(tmp_portd, LED_DINSYNC4_BIT);
      break;
  }

  PORTC |= tmp_portc; // Set RUN signal.
  PORTD |= tmp_portd; // Set LEDs.
  
  bitSet(playEnabled, port);
  bitClear(playPending, port);
}

void handleDinsyncStop(int port) {
  tmp_portc = 255;
  tmp_portd = 255;
  switch (port) {
    case 1:
      bitClear(tmp_portc, DINSYNC_RUN1_BIT);
      bitClear(tmp_portd, LED_DINSYNC1_BIT);
      break;
    case 2:
      bitClear(tmp_portc, DINSYNC_RUN2_BIT);
      bitClear(tmp_portd, LED_DINSYNC2_BIT);
      break;
    case 3:
      bitClear(tmp_portc, DINSYNC_RUN3_BIT);
      bitClear(tmp_portd, LED_DINSYNC3_BIT);
      break;
    case 4:
      bitClear(tmp_portc, DINSYNC_RUN4_BIT);
      bitClear(tmp_portd, LED_DINSYNC4_BIT);
      break;
  }

  PORTC &= tmp_portc;
  PORTD &= tmp_portd;
  
  bitClear(playEnabled, port);
  bitClear(playPending, port);
}

void startAllDinsync() {
  if (dinsyncClockHigh) { // Destroy any active clocks. 
    setDinsyncClockLow();
  }
  dinsyncNeedsDelay = true;
  
  // Set all RUN signals HIGH.
  PORTC |= B00011110;
  // Light up all RUN LEDs.
  PORTD |= B11110000;
  playEnabled |= B00011110;
  playPending &= B00011110;
}

void stopAllDinsync() {
  // Set all RUN signals LOW.
  PORTC &= B11100001;
  // Turn off all RUN LEDs.
  PORTD &= B00001111;
  playEnabled &= B11100001;
  playPending &= B11100001;
}

void setDinsyncClockLow() {
  dinsyncClockHigh = false;
  PORTC &= B11011111;
}

void setDinsyncClockHigh() {
  if (!dinsyncNeedsDelay) { // Only send the clock when no artificial delay is needed.
    dinsyncTimeOut = 0;
    dinsyncClockHigh = true;
    PORTC |= B00100000;
  }
}
