#include <TimerOne.h>
#include <MsTimer2.h>
#include <MIDI.h>
#include <ezButton.h>


// Create and bind the MIDI interface to the default hardware Serial port
MIDI_CREATE_DEFAULT_INSTANCE();

//#define DEBUG

#define LED_GLOBAL_PIN 2
#define LED_MIDI_PIN 3
#define LED_DINSYNC1_PIN 4
#define LED_DINSYNC2_PIN 5
#define LED_DINSYNC3_PIN 6
#define LED_DINSYNC4_PIN 7

#define LED_GLOBAL_BIT 2
#define LED_MIDI_BIT 3
#define LED_DINSYNC1_BIT 4
#define LED_DINSYNC2_BIT 5
#define LED_DINSYNC3_BIT 6
#define LED_DINSYNC4_BIT 7

#define BTN_GLOBAL_PIN 8
#define BTN_MIDI_PIN 9
#define BTN_DINSYNC1_PIN 10
#define BTN_DINSYNC2_PIN 11
#define BTN_DINSYNC3_PIN 12
#define BTN_DINSYNC4_PIN 13

#define BTN_SHIFT_PIN A0
#define DINSYNC_RUN1 A1
#define DINSYNC_RUN2 A2
#define DINSYNC_RUN3 A3
#define DINSYNC_RUN4 A4
#define DINSYNC_CLK A5

#define DINSYNC_RUN1_BIT 1
#define DINSYNC_RUN2_BIT 2
#define DINSYNC_RUN3_BIT 3
#define DINSYNC_RUN4_BIT 4
#define DINSYNC_CLK_BIT 5

#define EXTERNAL_SYNC_TIMOUT 2000 // 2s
#define MIDISYNC_START_DELAY 1 // 1ms
#define DINSYNC_PULSE_WIDTH 4 // 4ms
#define DINSYNC_START_DELAY 2 // 2ms
#define DEBOUNCE_DELAY 50L
#define PPQN 24 // Total number of pulses per quarter note, which is 24 for MIDI and DINSYNC.
#define DIV_MEASURE 12 // Time signature in eights gives 12 pulses as smallest division.
#define TEMPO_CALC_BUFFER_SIZE 10 // Tempo calc buffer size, 10 because easy maths.

ezButton btn_global(BTN_GLOBAL_PIN);
ezButton btn_midi(BTN_MIDI_PIN);
ezButton btn_dinsync1(BTN_DINSYNC1_PIN);
ezButton btn_dinsync2(BTN_DINSYNC2_PIN);
ezButton btn_dinsync3(BTN_DINSYNC3_PIN);
ezButton btn_dinsync4(BTN_DINSYNC4_PIN);
ezButton btn_shift(BTN_SHIFT_PIN);

int buttonState = LOW;
int lastButtonState = LOW; // The previous button state.
int pulseCount = 0; // Counts 24 pulses from 0..23.
int countEights = 0; // Counts up to int signature;
int signature = 8; // Time signature nominator setting. Denominator is fixed to 8 (8/8 => 4/4 time). TODO: Allow setting a range up to 16.
int bpm = 1200; // Tempo in tenths of a bpm.
int clockTimeOut = 0; // Counts milliseconds up to EXTERNAL_SYNC_TIMOUT.
int midisyncStartTimeout = 0; // Counts milliseconds up to MIDISYNC_START_DELAY.
int dinsyncStartTimeout = 0; // Count milliseconds up to DINSYNC_START_DELAY.
int dinsyncTimeOut = 0; // Counts milliseconds up to DINSYNC_PULSE_WIDTH.
int tempoCalcBuffer[TEMPO_CALC_BUFFER_SIZE]; // Tempo delta's are stored in a buffer with moving index.
int tempoCalcBufferIndex = 0; // The index of the buffer;
int tempoCalcBufferTotal = 0; // The total of the buffer;

byte tmp_portb = 0; // Used as global byte to facilitate writing to ports.
byte tmp_portc = 0; // Used as global byte to facilitate writing to ports.
byte tmp_portd = 0; // Used as global byte to facilitate writing to ports.
// The bytes below are used to store information about the sync outs. Only the 5 right bits are used for respectively D4, D3, D2, D1, M1.
byte playEnabled = 0; // Stores which outputs are playing.
byte playPending = 0; // Stores which outpunts are pending to play.

boolean internalSync = true;
boolean isPlaying = false;
boolean wantsToPlay = false;
boolean playMoment = false;  // When this becomes true, it's a good moment to start any pending clocks.
boolean midisyncNeedsDelay = false;
boolean dinsyncClockHigh = false;
boolean dinsyncNeedsDelay = false;
boolean tempoCalcBufferReady = false;

volatile byte flagPulse = 0; // Becomes 1 if we advance 1 pulse.
volatile byte flagQuarterNote = 0; // Becomes 1 if we advance 1 quarter note.
volatile byte flagMeasure = 0; // Becomes 1 if we advance DIV_MEASURE pulses.
volatile byte flagStart = 0; // Becomes 1 when start is received. Used to reset pulseCount
volatile byte flagMs = 0; // Becomes 1 when 1ms has passed.
long tempoInterval; // Interval for interupt in microseconds.

unsigned long currentBeatTime = 0; // Used for BPM calc when using external clock
unsigned long lastBeatTime = 0; // Used for BPM calc when using external clock

void setup() {

  pinMode(LED_GLOBAL_PIN, OUTPUT);
  pinMode(LED_MIDI_PIN, OUTPUT);
  pinMode(LED_DINSYNC1_PIN, OUTPUT);
  pinMode(LED_DINSYNC2_PIN, OUTPUT);
  pinMode(LED_DINSYNC3_PIN, OUTPUT);
  pinMode(LED_DINSYNC4_PIN, OUTPUT);

  btn_global.setDebounceTime(DEBOUNCE_DELAY);
  btn_midi.setDebounceTime(DEBOUNCE_DELAY);
  btn_dinsync1.setDebounceTime(DEBOUNCE_DELAY);
  btn_dinsync2.setDebounceTime(DEBOUNCE_DELAY);
  btn_dinsync3.setDebounceTime(DEBOUNCE_DELAY);
  btn_dinsync4.setDebounceTime(DEBOUNCE_DELAY);
  btn_shift.setDebounceTime(DEBOUNCE_DELAY);

  pinMode(DINSYNC_RUN1, OUTPUT);
  pinMode(DINSYNC_RUN2, OUTPUT);
  pinMode(DINSYNC_RUN3, OUTPUT);
  pinMode(DINSYNC_RUN4, OUTPUT);
  pinMode(DINSYNC_CLK, OUTPUT);

  // Set initial LED states.
  PORTD &= B11000000;

  // Set output states.
  PORTC &= B10000011;

  Timer1.initialize(calcInterval(bpm));
  Timer1.attachInterrupt(countPulse);

  MsTimer2::set(1L, countTime);
  MsTimer2::start();

  MIDI.setHandleClock(handleExtClock);
  MIDI.setHandleStart(handleExtStart);
  //MIDI.setHandleContinue(handleExtContinue); // TODO: Do something with continue?
  MIDI.setHandleStop(handleExtStop);
  MIDI.begin(MIDI_CHANNEL_OMNI);  // Listen to all incoming messages. TODO: perhaps a filter can be used?
  MIDI.turnThruOff();

  #ifdef DEBUG
    Serial.begin(31250);
    Serial.println("START DEBUG");
  #endif
}

void handleClock() {
  // Reset timeout.
  clockTimeOut = 0;

  if (!dinsyncClockHigh) { // Don't send a clock pulse when there is already a clock pulse playing.
    // Send a midi clock.
    sendMidiClock();
  
    // Send a dinsync clock.
    setDinsyncClockHigh();
  }
}

void handleStart() {
  isPlaying = true;
  handleMidiStart();
  startAllDinsync();
}

void handleStop() {
  isPlaying = false;
  handleMidiStop();
  stopAllDinsync();
}

void resetCounters() {

  #ifdef DEBUG
    Serial.println("");
    Serial.println(pulseCount);
    Serial.println(countEights);
    Serial.println(midisyncStartTimeout);
    Serial.println(dinsyncStartTimeout);
    Serial.println("");
  #endif

  pulseCount = 0;
  countEights = 0;

  // Reset timeouts
  midisyncStartTimeout = 0;
  dinsyncStartTimeout = 0;
}

void switchToInternalSync() {
  internalSync = true;
  updateBPM();
  Timer1.setPeriod(calcInterval(bpm));
  Timer1.restart(); 
}

// Calculate the interval for the tempo interrupt.
long calcInterval(int bpm) {
  return 60L * 1000 * 1000 * 10 / bpm / PPQN;
}

void updateBPM() {
  // TODO FIX THIS LATER
  /*
  if (tempoCalcBufferReady) { // With a full buffer of 10 items, the calculation is a bit simpler.
    bpm = round(60000 / tempoCalcBufferTotal);
  }
  else if (tempoCalcBufferTotal > 0) { // If not we can do this.
    int buffersize = 0;
    for (int i = 0; i < TEMPO_CALC_BUFFER_SIZE; i++) {
      if (tempoCalcBuffer[i] != 0) {
        buffersize++;
      }
    }
    if (buffersize > 0) {
      bpm = round(600000 / tempoCalcBufferTotal / buffersize);
    }
  }
  */
}
