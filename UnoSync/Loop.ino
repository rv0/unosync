void loop() {
  // Read incoming midi messages.
  MIDI.read();

  // 1ms has passed.
  if (flagMs) {
    flagMs = 0;
    updateTimeCounters();
  }

  // Switch to internal sync if external sync is not present.
  if (!internalSync && clockTimeOut >= EXTERNAL_SYNC_TIMOUT) {
    switchToInternalSync();
  }

  // Older dinsync devices will miss the first clock tick if it hapens too close to run signal edge.
  // As a way to work with this, any ticks during start are cancelled automatically.
  // So this tick is blocked in setDinsyncClockHigh() until DINSYNC_START_DELAY time is reached.
  // After that we send it here. This leads to a negligible jitter on all dinsync outputs for only the first clock tick.
  if (dinsyncNeedsDelay && dinsyncStartTimeout >= DINSYNC_START_DELAY) {
    dinsyncNeedsDelay = false;
    dinsyncStartTimeout = 0;
    setDinsyncClockHigh();
  }

  // Similarly, midi devices prefer some time between the first midi start message and the following midi clock byte.
  if (midisyncNeedsDelay && midisyncStartTimeout >= MIDISYNC_START_DELAY) {
    midisyncNeedsDelay = false;
    midisyncStartTimeout = 0;
    sendMidiClock();
  }
  

  // Turn Dinsync clock low after DINSYNC_PULSE_WIDTH time.
  if (dinsyncClockHigh && dinsyncTimeOut >= DINSYNC_PULSE_WIDTH) {
    setDinsyncClockLow();
  }

  // Poll buttons.
  pollButtons();

  if (btn_global.isPressed()) {
    handleGlobalButtonPressed();
  }

  if (btn_midi.isPressed()) {
    handleMidiButtonPressed();
  }

  if (btn_dinsync1.isPressed()) {
    handleDinsyncButtonPressed(1);
  }

  if (btn_dinsync2.isPressed()) {
    handleDinsyncButtonPressed(2);
  }

  if (btn_dinsync3.isPressed()) {
    handleDinsyncButtonPressed(3);
  }

  if (btn_dinsync4.isPressed()) {
    handleDinsyncButtonPressed(4);
  }

  // Respond to advances of the clock.
  if (flagPulse) {
    flagPulse = 0;

    // Handle any pending plays.
    if (playMoment) {
      playMoment = false;  

      #ifdef DEBUG
        Serial.println(""); // Start new line.
      #endif

      if (!isPlaying && wantsToPlay) { // Activate global play that is pending (when using external clock).
        wantsToPlay = false;
        
        #ifdef DEBUG
          Serial.println("wantsToPlay");
        #endif
        
        handleStart();  
      }
      else if (playPending > 0) { // Activate individual plays that are pending.
        if (bitRead(playPending, 0)) { // MIDI 1.
          handleMidiStart();
        }
        for (int i = 1; i <= 4; i++) { // DINSYNC 1..4.
          if (bitRead(playPending, i)) {
            handleDinsyncStart(i); // TODO: Change this to single call.
          }
        }
      }
    }

    if (flagMeasure) { // We advanced 12 pulses
      flagMeasure = 0;

      #ifdef DEBUG
        Serial.print(countEights);
      #endif
 
      countEights++;
      if (countEights >= signature) {
        countEights = 0;
        playMoment = true; // Will be handled on next pulse.
      }
    }

    if (flagQuarterNote) { // We advanced 24 pulses
      flagQuarterNote = 0;

      // Time between now and previous measure is stored in a cyclic buffer for reliable avarage tempo approximation
      // EXPERIMENTAL: Not used at this moment.
      /*
      currentBeatTime = millis();
      if (currentBeatTime > lastBeatTime && currentBeatTime - lastBeatTime > 0) {
        if (tempoCalcBuffer[tempoCalcBufferIndex] > 0) {
          tempoCalcBufferTotal -= tempoCalcBuffer[tempoCalcBufferIndex];
        }
        tempoCalcBuffer[tempoCalcBufferIndex] = currentBeatTime - lastBeatTime;
        tempoCalcBufferTotal += tempoCalcBuffer[tempoCalcBufferIndex];
        tempoCalcBufferIndex++;
        if (tempoCalcBufferIndex == TEMPO_CALC_BUFFER_SIZE) {
          tempoCalcBufferIndex = 0;
          tempoCalcBufferReady = true; // Only matters if the buffer was never filled before.
        }
      }
      lastBeatTime = currentBeatTime;
      */
    }

    if (internalSync) {
      handleClock();
    }

    // Tempo-synced LED blinking.
    if (pulseCount >= 12) {
      tmp_portd = 0;
      
      // Always blink global led.
      bitSet(tmp_portd, LED_GLOBAL_BIT);
      
      // Blink all individual leds in case of global play.
      if (wantsToPlay) {
        tmp_portd |= B11111000;
      } 
      // Or blink individuals LEDs of output which are pending to play.
      else if (playPending > 0) {
        if (bitRead(playPending, 0)) { // MIDI 1.
          bitSet(tmp_portd, LED_MIDI_BIT);
        }
        if (bitRead(playPending, 1)) { // DINSYNC 1.
          bitSet(tmp_portd, LED_DINSYNC1_BIT);
        }
        if (bitRead(playPending, 2)) { // DINSYNC 2.
          bitSet(tmp_portd, LED_DINSYNC2_BIT);
        }
        if (bitRead(playPending, 3)) { // DINSYNC 3.
          bitSet(tmp_portd, LED_DINSYNC3_BIT);
        }
        if (bitRead(playPending, 4)) { // DINSYNC 4.
          bitSet(tmp_portd, LED_DINSYNC4_BIT);
        }
      }
      PORTD |= tmp_portd;
    }
    else {
      tmp_portd = 255;

      // Always blink global led.
      bitClear(tmp_portd, LED_GLOBAL_BIT);

      // Blink all leds in case of global play.
      if (wantsToPlay) {
        tmp_portd &= B00000111;
      } 
      // Or blink individuals LEDs of output which are pending to play.
      else if (playPending > 0) {
        if (bitRead(playPending, 0)) { // MIDI 1.
           bitClear(tmp_portd, LED_MIDI_BIT);
        }
        if (bitRead(playPending, 1)) { // DINSYNC 1.
          bitClear(tmp_portd, LED_DINSYNC1_BIT);
        }
        if (bitRead(playPending, 2)) { // DINSYNC 2.
          bitClear(tmp_portd, LED_DINSYNC2_BIT);
        }
        if (bitRead(playPending, 3)) { // DINSYNC 3.
          bitClear(tmp_portd, LED_DINSYNC3_BIT);
        }
        if (bitRead(playPending, 4)) { // DINSYNC 4.
          bitClear(tmp_portd, LED_DINSYNC4_BIT);
        }
      }
      PORTD &= tmp_portd;
    }
  }
}
