void handleMidiStart() {
  MIDI.sendStart();
  // Turn MIDI LED on.
  PORTD |= B00001000;
  midisyncNeedsDelay = true;
  bitSet(playEnabled, 0);
  bitClear(playPending, 0);
}

void handleMidiStop() { // Handles stopping of the Midi output.
  MIDI.sendStop();
  // Turn MIDI LED off.
  PORTD &= B11110111;
  bitClear(playEnabled, 0);
  bitClear(playPending, 0);
}

void handleExtStart() {
  flagStart = 1;
  resetCounters();
  handleStart();
}

void handleExtStop() {
  handleStop();
}

void handleExtClock() {
  if (internalSync) {
    internalSync = false;
    Timer1.stop();
  }
  countPulse();
  handleClock();
}


void sendMidiClock() {
  if (!midisyncNeedsDelay) { // Only send the clock when no artificial delay is needed.
    #ifndef DEBUG // Don't send midi clocks in debug mode as they spam the serial console.
      MIDI.sendClock();
    #endif
  }
}
