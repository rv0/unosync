void handleGlobalButtonPressed() {
  if (isPlaying) {
    handleStop();
  }
  else {
    if (internalSync) {
      resetCounters();
      handleStart();
    }
    else {
      wantsToPlay = true;
      playPending = 0; // No need to activate any individual plays.
    }
  }
}

void handleMidiButtonPressed() {
  if (bitRead(playEnabled, 0)) {
    handleMidiStop();
  }
  else {
    bitSet(playPending, 0);
  }
}

void handleDinsyncButtonPressed(int port) {
  if (bitRead(playEnabled, port)) {
    handleDinsyncStop(port);
  }
  else {
    bitSet(playPending, port);
  }
}

void pollButtons() {
  btn_global.loop();
  btn_midi.loop();
  btn_dinsync1.loop();
  btn_dinsync2.loop();
  btn_dinsync3.loop();
  btn_dinsync4.loop();
  btn_shift.loop();
}
