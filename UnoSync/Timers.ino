// Callback from internal clock or midi clock counting the pulses in a quarter note.
// Increments pulseCount ranging from 1..24. (depending on PPQN setting)
// Sets appropriate flags that are used in the main loop.
void countPulse() {
  flagPulse = 1;

  pulseCount++;
  if (pulseCount == DIV_MEASURE || pulseCount == PPQN) {
    flagMeasure = 1;
    if (pulseCount == PPQN) {
      pulseCount = 0;
      flagQuarterNote = 1;
    }
  }
}

// Timer callback for time in milliseconds.
void countTime() {
  flagMs = 1;
}

// Restarts the milisecond timer.
void restartMsTimer() {
  MsTimer2::stop();
  MsTimer2::start();
}

// Called from main loop, gets called every millisecond based on above timer.
void updateTimeCounters() {
  if (!internalSync) {
    clockTimeOut++;
  } 

  if (midisyncNeedsDelay) {
    midisyncStartTimeout++;
  }

  if (dinsyncClockHigh) {
    dinsyncTimeOut++;
  }
  else if (dinsyncNeedsDelay) {
    dinsyncStartTimeout++;
  }
}
